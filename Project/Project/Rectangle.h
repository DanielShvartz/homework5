#pragma once
#include "Polygon.h"

namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	private:
		Point _point;
		double _length;
		double _width;
	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const string& type, const string& name);
		virtual ~Rectangle();

		// we didnt had any drawing functions so we added by overidng
		virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
		virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

		// override functions if need (virtual + pure virtual)
		virtual void move(const Point& other);
		virtual double getArea() const;
		virtual double getPerimeter() const;
	};
}