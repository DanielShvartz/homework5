#pragma once
#include "Shape.h"
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"

#include <vector>
#include <iostream>
#include <sstream>

#define TYPE_CIR "circle"
#define TYPE_ARR "arrow"
#define TYPE_TRI "triangle"
#define TYPE_REC "rectangle"

#define NEW_SHAPE 0
#define MODIFY 1
#define DELETE_ALL 2
#define EXIT 3

#define CIRCLE 0
#define ARROW 1
#define TRIANGLE 2
#define RECTANGLE 3
#define BACK 4

#define MOVE 0
#define DETAILS 1
#define REMOVE 2
#define MENU_BACK 3

class Menu
{
public:

	Menu(int t);
	~Menu();

private:
	std::vector<Shape*> _shapes; // its like array of shapes

	void clear() const; // clear the screen
	void pause() const;

	int printMenu();
	int createShape();

	/* The function lets the user choose one of the shapes and modify it
	Output: '1' to stay on this menu, '0' neither
	*/
	int modifyMenu();
	void deleteAllShapes();
	/* The function removes the shape
	Input: index of the shape in the vector '_shapes' | 'true' to ask the user if he sure, 'false' neither
	Output: 'true' if the user chose to remove the shape, 'false' neither
	*/
	boolean removeShape(int index, boolean ask);

	void createCircle();
	void createArrow();
	void createTriangle();
	void createRectangle();

	void move(Shape* shape);

	void clearBoard();
	// The function redraws the board from the vector '_shapes'
	void redrawBoard();

	typedef struct shapesMenu 
	{
		shapesMenu() // we create struct that will help us using the menu
		{
			exit = false; // dont exit
			shape = nullptr; //  we start with no shape
			index = 0; // index and size are zerod
			size = 0; // size will get bigger each time a shape creats
		}
		boolean exit; // if to exit or not
		Shape* shape; // our base
		int index; // index of the wanted shape
		int size; // the size of the array
	} shapesMenuRet;

	/* The function prints all the shapes in the vector '_shapes' and let the user choose one of them
	Output: shapesMenuRet
	*/
	shapesMenuRet shapesMenu();

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

