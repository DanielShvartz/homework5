#include "Menu.h"


Menu::Menu(int t)
{
	int open = 1; // we start the loop with one and if we stop we get out of the loop
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");

	while (open) {
		open = printMenu(); // start the menu 
	}
}
Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

int Menu::printMenu() 
{
	int input = 0, looper = 0;

	clear(); // we clear the board and print the menu
	std::cout << "Enter 0 to add a new shape." << std::endl;
	std::cout << "Enter 1 to modify or get information from a current shape." << std::endl;
	std::cout << "Enter 2 to delete all the shapes." << std::endl;
	std::cout << "Enter 3 to exit." << std::endl;
	std::cin >> input;

	switch (input) 
	{
	case NEW_SHAPE:
		looper = 1;
		while (looper)
			looper = createShape();
		break;

	case MODIFY:
		looper = 1;
		while (looper)
			looper = modifyMenu();
		break;

	case DELETE_ALL:
		deleteAllShapes();
		break;

	case EXIT:
		return 0;
		break;

	default:
		std::cout << "** Invalid input" << std::endl;
		pause();
		break;
	}

	return 1;
}

int Menu::createShape() 
{
	int input = 0;

	clear();
	std::cout << "Enter 0 to add a circle." << std::endl;
	std::cout << "Enter 1 to add an arrow." << std::endl;
	std::cout << "Enter 2 to add a triangle." << std::endl;
	std::cout << "Enter 3 to add a rectangle." << std::endl;
	std::cout << "Enter 4 to go back." << std::endl;
	std::cin >> input;

	switch (input) 
	{
	case CIRCLE:
		createCircle();
		break;

	case ARROW:
		createArrow();
		break;

	case TRIANGLE:
		createTriangle();
		break;

	case RECTANGLE:
		createRectangle();
		break;

	case BACK:
		return 0;
		break;

	default:
		std::cout << "** Invalid input" << std::endl;
		pause();
		return 1;
		break;
	}

	return 0;
}

/*
The function prints the menu of shape midification
*/
int Menu::modifyMenu() 
{
	int input = 0;
	boolean flag = false;

	shapesMenuRet shapeMenu = shapesMenu();
	Shape* shape = shapeMenu.shape;

	clear();

	if (shapeMenu.exit) 
		return 0;
	if (shapeMenu.size == 0 || shape == nullptr) // to have no shapes we need to have no shapes 
	{
		std::cout << "You don't have any shapes" << std::endl;
		pause();
		return 0;
	}

	while (!flag) 
	{
		clear();

		std::cout << shape->getName() << "(" << shape->getType() << "):" << std::endl;
		std::cout << "Enter 0 to move the shape." << std::endl;
		std::cout << "Enter 1 to get its details." << std::endl;
		std::cout << "Enter 2 to remove the shape." << std::endl;
		std::cout << "Enter 3 to go back." << std::endl;
		std::cin >> input;

		switch (input) 
		{
		case MOVE:
			move(shape);
			break;

		case DETAILS:
			shape->printDetails();
			pause();
			break;

		case REMOVE:
			if (removeShape(shapeMenu.index, true))  // if he wants to remove a shape we remove it by its index
			{
				flag = true; // and stop this menu loop
				return 0; // if we return 0 it means we dont run
			}
			break;

		case MENU_BACK:
			flag = true;
			return 1; // if it returns 1 it means we run back
			break;

		default:
			std::cout << "** Invalid input" << std::endl;
			pause();
			break;
		}
	}

	return 0;
}

/*
The function deletes all the shapes in the array of shapes it makes sure he want to delete em
*/
void Menu::deleteAllShapes() 
{
	char input = 0;

	clear();

	std::cout << "Are you sure you want to delete all the shpes?" << endl;
	std::cout << "'Y' to continue, any else key to cancel" << endl;
	std::cin >> input;

	if (input == 'Y') // if he choose to delete the shapes
		while (_shapes.size() > 0) // we run until we're not having any more shapes
			removeShape(0, false); // and we say the we want to remove the shape and we're sure

}

/*
The function removes a shape
gets an index of the wanted shape to remove and if he sure
*/
boolean Menu::removeShape(int index, boolean ask) 
{
	Shape* shape = nullptr;
	char input = 0;

	if (index >= 0 && index < _shapes.size()) // if the index is smaller then the 0 or bigger then the shape size we cant run 
	{

		if (ask) 
		{
			clear(); // we ask the user if he wants to remove the sahpe
			shape = _shapes[index];

			std::cout << "Are you sure you want to remove " << shape->getName() << "(" << shape->getType() << ")?" << endl;
			std::cout << "'Y' to continue, any else key to cancel" << endl;
			std::cin >> input;
		}

		if (input == 'Y' || !ask)  // if he chose to clear the shape
		{

			clearBoard(); // we clear the board
			delete _shapes[index]; // remove it
			_shapes[index] = nullptr; // set the point as null
			_shapes.erase(_shapes.begin() + index); // and we move back all the shapes
			redrawBoard(); // then we redraw all the shapes

			return true;
		}
	}

	return false;
}

void Menu::createCircle() 
{
	double x = 0, y = 0, r = 0;
	string name = "";

	std::cout << "Please enter X: ";
	std::cin >> x;

	std::cout << "Please enter Y: ";
	std::cin >> y;

	std::cout << "Please enter radius: ";
	std::cin >> r;

	std::cout << "Please enter the name of the circle: ";
	std::cin >> name;

	Circle* c = new Circle(Point(x, y), r, TYPE_CIR, name);
	c->draw(*_disp, *_board);
	_shapes.push_back(c);
}

void Menu::createArrow() 
{
	double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
	string name = "";

	std::cout << "Please enter the X of the 1st point: ";
	std::cin >> x1;

	std::cout << "Please enter the Y of the 1st point: ";
	std::cin >> y1;

	std::cout << "Please enter the X of the 2nd point: ";
	std::cin >> x2;

	std::cout << "Please enter the Y of the 2nd point: ";
	std::cin >> y2;

	std::cout << "Please enter the name of the arrow: ";
	std::cin >> name;

	Arrow* a = new Arrow(Point(x1, y1), Point(x2, y2), TYPE_ARR, name);
	a->draw(*_disp, *_board);
	_shapes.push_back(a);
}

void Menu::createTriangle() 
{
	double x1 = 0, y1 = 0, x2 = 0, y2 = 0, x3 = 0, y3 = 0;
	string name = "";

	std::cout << "Please enter the X of the 1st point: ";
	std::cin >> x1;

	std::cout << "Please enter the Y of the 1st point: ";
	std::cin >> y1;

	std::cout << "Please enter the X of the 2nd point: ";
	std::cin >> x2;

	std::cout << "Please enter the Y of the 2nd point: ";
	std::cin >> y2;

	std::cout << "Please enter the X of the 3rd point: ";
	std::cin >> x3;

	std::cout << "Please enter the Y of the 3rd point: ";
	std::cin >> y3;

	std::cout << "Please enter the name of the triangle: ";
	std::cin >> name;

	Triangle* t = new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), TYPE_TRI, name);
	if (t->getError()) 
	{
		std::cout << "\n" << t->getErrorDes() << endl;
		pause();
	}
	else 
	{
		t->draw(*_disp, *_board);
		_shapes.push_back(t);
	}
}

void Menu::createRectangle() 
{
	double x = 0, y = 0, length = 0, width = 0;
	string name = "";

	std::cout << "Please enter the X of the top-left corner: ";
	std::cin >> x;

	std::cout << "Please enter the Y of the top-left corner: ";
	std::cin >> y;

	std::cout << "Please enter the length of the rectangle: ";
	std::cin >> length;

	std::cout << "Please enter the width of the rectangle: ";
	std::cin >> width;

	std::cout << "Please enter the name of the rectangle: ";
	std::cin >> name;

	myShapes::Rectangle* r = new myShapes::Rectangle(Point(x, y), length, width, TYPE_REC, name);
	if (r->getError()) 
	{
		std::cout << "\n" << r->getErrorDes() << endl;
		pause();
	}
	else 
	{
		r->draw(*_disp, *_board);
		_shapes.push_back(r);
	}
}

void Menu::move(Shape* shape) 
{
	double x = 0;
	double y = 0;

	clear();

	std::cout << "Please enter the X moving scale: ";
	std::cin >> x;
	std::cout << "Please enter thr Y moving scale: ";
	std::cin >> y;

	Point newP(x, y);
	clearBoard();
	shape->move(newP);
	redrawBoard();
}

void Menu::clearBoard() 
{
	for (int i = 0; i < _shapes.size(); i++) 
		_shapes[i]->clearDraw(*_disp, *_board);
}

void Menu::redrawBoard() 
{
	for (int i = 0; i < _shapes.size(); i++)
		_shapes[i]->draw(*_disp, *_board);
}

Menu::shapesMenuRet Menu::shapesMenu() 
{
	shapesMenuRet ret;
	Shape* shape = nullptr;

	int input = 0;

	// Loops
	int i = 0;

	// Flags
	boolean flag = false;

	if (_shapes.size() == 0)
		return ret;
	clear();

	for (i = 0; i < _shapes.size(); i++) 
	{
		shape = _shapes[i];
		std::cout << "Enter " << i << " for " << shape->getName() << "(" << shape->getType() << ")." << std::endl;
	}

	std::cout << "Enter " << _shapes.size() << " to go back." << std::endl;

	while (!flag) 
	{
		flag = true;

		std::cin >> input;

		if (input >= 0 && input < _shapes.size()) 
		{
			ret.index = input;
			ret.shape = _shapes[input];
			ret.size = _shapes.size();
		}
		else if (input == _shapes.size()) 
			ret.exit = true;
		else 
		{
			flag = false;
			std::cout << "Invalid input. Try again: ";
		}
	}

	return ret;
}


void Menu::clear() const 
{
	system("CLS");
}

void Menu::pause() const 
{
	std::cout << std::endl;
	system("PAUSE");
}
