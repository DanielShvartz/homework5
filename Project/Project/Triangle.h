#pragma once
#include "Polygon.h"

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

	virtual void move(const Point& point);
	virtual double getArea() const;
	virtual double getPerimeter() const;

private:
	Point _p1;
	Point _p2;
	Point _p3;
	// override functions if need (virtual + pure virtual)
};