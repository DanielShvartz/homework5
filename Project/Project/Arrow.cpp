#include "Arrow.h"

void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char COLOR[] = { 255, 0, 0 };
	std::cout << std::endl;
	this->colorMenu(COLOR);
	// if wanted to draw we send all the points
	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), COLOR, 100.0f).display(disp);

}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	// to clear the draw we draw it black on black
	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}

/*
C'tor thats intialize a shape and then two points
*/
Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name) : Shape(name, type)
{
	_p1 = a;
	_p2 = b;
}
Arrow::~Arrow()
{

}
/*
The function moves an arrow by given x and y
input; new point moves
output; moves the arrow
*/
void Arrow::move(const Point& other)
{
	_p1 += other;
	_p1 += other;
}
/*
The function returns a perimeter of an arrow
the perimeter of an arrow is the distance which we already done between two given points
*/
double Arrow::getPerimeter() const
{
	return _p1.distance(_p2);
}
double Arrow::getArea() const
{
	return 0;
}