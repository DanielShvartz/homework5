#include "Point.h"
/*
Added defualt c'tor
*/
Point::Point()
{

}
Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}
/*
Copy c'tor
Gets the pointers and copys x and y
*/
Point::Point(const Point& other)
{
	_x = other.getX();
	_y = other.getY();
}
/*
Virtual d'tor
just clears the object
*/
Point::~Point()
{

}
/*
The function adds two points
input; two points
output; the function creates and returns a new point with the added points values
*/
Point Point::operator+(const Point& other) const
{
	return Point(_x + other.getX(), _y + other.getY());
}
Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	return *this;
}
double Point::getX() const
{
	return _x;
}
double Point::getY() const
{
	return _y;
}
double Point::distance(const Point& other) const
{
	return sqrt(pow((_x - other.getX()), 2) + pow((_y - other.getY()), 2));
}