#include "Triangle.h"
void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

Triangle::Triangle(const Point & a, const Point & b, const Point & c, const string & type, const string & name) : Polygon(name, type) {
	if (!(a.getX() == b.getX() && b.getX() == c.getX()) && !(a.getY() == b.getY() && b.getY() == c.getY())) 
	{
		_p1 = a;
		_p2 = b;
		_p3 = c;

		this->_points.push_back(a);
		this->_points.push_back(b);
		this->_points.push_back(c);
	}
	else 
	{
		_error = true;
		this->_error_des = "All the points can't be on the same axis";
	}
}
Triangle::~Triangle()
{

}
void Triangle::move(const Point& point)
{
	_p1 += point;
	_p2 += point;
	_p3 += point;
}
double Triangle::getArea() const
{
	Point center((_p2 + _p3).getX() / 2, (_p2 + _p3).getY() / 2);
	return (_p1.distance(center) * _p2.distance(_p3)) / 2;
}
double Triangle::getPerimeter() const
{
	// the distance of each two points 3 times between them create the perimeter
	return _p1.distance(_p2) + _p2.distance(_p3) + _p3.distance(_p1);
}