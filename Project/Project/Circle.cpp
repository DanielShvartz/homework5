#include "Circle.h"

void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char COLOR[] = { 0, 0, 255 };
	std::cout << std::endl;
	this->colorMenu(COLOR); // if he wants to drwa the circle we ask for a color
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), COLOR, 100.0f).display(disp);

}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}

Circle::Circle(const Point& center, double radius, const string& type, const string& name) : Shape(name, type)
{
	_center = center;
	_radius = radius;
}
Circle::~Circle()
{

}
const Point& Circle::getCenter() const
{
	return _center;
}
double Circle::getRadius() const
{
	return _radius;
}
void Circle::move(const Point& other)
{
	_center += other;
}
double Circle::getArea() const
{
	//PI * R^2
	return PI * pow(_radius, 2);
}
double Circle::getPerimeter() const
{
	//PI * 2 * R
	return 2 * PI * _radius;
}