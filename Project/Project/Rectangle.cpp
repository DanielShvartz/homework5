#include "Rectangle.h"

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char COLOR[] = { 255, 255, 255 };
	std::cout << std::endl;
	colorMenu(COLOR);
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), COLOR, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);

}


myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) : Polygon(name, type)
{
	if (length > 0 && width > 0) 
	{
		_point = a;
		_length = length;
		_width = width;
		_points.push_back(a + Point(0, length)); // when we create the shaoe we add the rectangle points
		_points.push_back(a + Point(width, 0));
	}
	else 
	{
		this->_error = true;
		this->_error_des = "The width and the length must to be bigger than 0";
	}
}
myShapes::Rectangle::~Rectangle()
{

}
void myShapes::Rectangle::move(const Point& other)
{
	_point += other;
	_points[0] += other;
	_points[1] += other;

}
double myShapes::Rectangle::getArea() const
{
	return _width * _length;
}
double myShapes::Rectangle::getPerimeter() const
{
	return (_width * 2) + (_length * 2);
}