#include "Shape.h"
#include <string>
Shape::Shape(const string& name, const string& type)
{
	_error = 0;
	_error_des = "";
	_name = name;
	_type = type;
}
string Shape::getType() const
{
	return _type;
}
string Shape::getName() const
{
	return _name;
}
void Shape::printDetails() const
{
	std::cout << _type << "\t" << _name << "\t" << getPerimeter() << "\t" << getArea() << std::endl;
}
Shape::Shape()
{

}
boolean Shape::getError() const 
{
	return this->_error;
}
string Shape::getErrorDes() const 
{
	return this->_error_des;
}
void Shape::colorMenu(unsigned char* color) {
	int input = 0;
	// Flags
	boolean flag = false;

	std::cout << "Choose color:" << std::endl;
	std::cout << "White - 0" << std::endl;
	std::cout << "Red - 1" << std::endl;
	std::cout << "Green - 2" << std::endl;
	std::cout << "Blue - 3" << std::endl;

	while (!flag) {
		flag = true;
		std::cin >> input;

		switch (input) {
		case 0:
			color[0] = 255;
			color[1] = 255;
			color[2] = 255;
			break;

		case 1:
			color[0] = 255;
			color[1] = 0;
			color[2] = 0;
			break;

		case 2:
			color[0] = 0;
			color[1] = 255;
			color[2] = 0;
			break;

		case 3:
			color[0] = 0;
			color[1] = 0;
			color[2] = 255;
			break;

		default:
			flag = false;
			std::cout << "Invalid input. Try again: ";
		}
	}
}
